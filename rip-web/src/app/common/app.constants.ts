export const AppConstants = {
  clientDateTimeFormat: {
    pipe: {
      date: 'dd/MM/yyyy',
      dateTime: 'dd/MM/yyyy HH:mm',
      time: 'HH:mm',
      locale: 'en-US'
    },
    moment: {
      date: 'DD-MM-YYYY',
      dateTime: 'DD-MM-YYYY HH:mm',
      dateTimeWithSeconds: 'DD-MM-YYYY HH:mm:ss',
      time: 'HH:mm'
    }
  },
  tableMessages: {
    emptyMessage: 'No data to display',
    totalMessage: 'Total items'
  },
  validation: {
    username: {
      pattern: '^[^:;"%\'\\[\\]=,]*$',
      minLength: 6
    },
    name: {
      pattern: /^[A-Za-zĐđŽžĆćČčŠš]*$/,
      minLength: 3
    },
    surname: {
      pattern: /^[A-Za-zĐđŽžĆćČčŠš]*$/,
      minLength: 3
    },
    password: {
      pattern: '^[^:;"%\'\\[\\]=,]*$',
      minLength: 6
    },
    email: {
      pattern: '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
    },
    phoneNumber: {
      pattern: /^[0-9]*$/,
      minLength: 8
    }
  },
  validationErrorMessages: {
    usernamePattern: 'Not valid username',
    namePattern: 'Not valid name',
    surnamePattern: 'Not valid surname',
    passwordPattern: 'Not valid password',
    emailPattern: 'Not valid email',
    minLength: 'Minimal length is ',
    numberPattern: 'Not valid number',
    required: 'Required'
  }
};
