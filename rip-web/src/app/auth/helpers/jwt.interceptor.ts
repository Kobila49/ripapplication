import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {


  constructor(private http: HttpClient) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let jsonReq: HttpRequest<any> = req.clone({
      setHeaders: {
        Authorization: `${localStorage.getItem('access_token')}`
      }
    });

    return next.handle(jsonReq);
  }
}
