import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Principal, User } from '@app/auth/models';

import { environment } from '@env/environment';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  readonly rootUrl = `${environment.ripApiUrl}`;
  readonly userUrl = this.rootUrl + '/login';


  private authSource = new BehaviorSubject<string>(localStorage.getItem('isAuthenticated'));
  currentAuth = this.authSource.asObservable();


  constructor(public http: HttpClient,
    private router: Router) {
  }

  getToken() {
    return localStorage.getItem('access_token');
  }



  loginUser(username: string, password: string): Observable<any> {
    return this.http.post<any>(this.userUrl, { username: username, password: password }, { observe: 'response' })
      .pipe(map((res: any) => {
        if (res) {
          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify({ username }));
          localStorage.setItem('access_token', JSON.stringify( res.headers.get('Authorization')).slice(1,-1));
          this.setAuthStatus('true');
        }
      }));
  }

  fetchUserDetails(username: String): Observable<any> {
      return this.http.get<any>(this.rootUrl+ '/users/'+ username);
  }

  modifyUser(user: User): Observable<User> {
    const url = this.rootUrl + '/users/modify/' + localStorage.getItem('uuid');
    return this.http.put<User>(url, user);
  }

  registerUser(user: User): Observable<User> {
    return this.http.post<User>(this.rootUrl + '/users/register', user);
  }

  preparePrincipalData(formData: any): Principal {
    const formModel = formData;
    const data: Principal = new Principal();
    data.username = formModel.username;
    data.password = formModel.password;

    return data;
  }

  prepareRegistrationData(formData: any): User {
    const formModel = formData;
    const data: User = new User();
    data.username = formModel.username;
    data.firstName = formModel.firstName;
    data.lastName = formModel.lastName;
    data.address = formModel.address;
    data.password = formModel.password;
    data.email = formModel.email;
    data.phoneNumber = formModel.phoneNumber;

    return data;
  }

  setAuthStatus(status: string) {
    localStorage.setItem('isAuthenticated', status);
  }

  logout(): Observable<any> {
    // remove user from local storage to log user out
    localStorage.clear();
    return this.http.get<any>(this.rootUrl+ "/logout");

  }

}
