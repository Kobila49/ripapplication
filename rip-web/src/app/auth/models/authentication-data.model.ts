export class AuthenticationData {
  access_token: string;
  token_type: string;
  expires_in: string;
  jti: string;
}
