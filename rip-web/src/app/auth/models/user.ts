export class User{
  public username: string;
  public firstName: string;
  public lastName: string;
  public address: string;
  public password: string;
  public email: string;
  public phoneNumber: string;
  public points: number;
}
