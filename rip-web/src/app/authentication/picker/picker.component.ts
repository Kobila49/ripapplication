import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/core';

@Component({
  selector: 'rip-picker',
  templateUrl: './picker.component.html',
  styleUrls: ['./picker.component.scss'],
  animations: [routerTransition]
})
export class PickerComponent implements OnInit {
  selection = [
    { link: 'login', label: 'Login' },
    { link: 'sign-up', label: 'Sign Up' },

  ];


  constructor() { }

  ngOnInit() {
  }

}
