import { NgModule } from '@angular/core';
import { AuthenticationRoutingModule } from '@app/authentication/authentication-routing.module';
import { SharedModule } from '@app/shared';
import { LoginComponent } from './login/login.component';
import { PickerComponent } from './picker/picker.component';
import { SignUpComponent } from './sign-up/sign-up.component';

@NgModule({
  imports: [
    SharedModule,
    AuthenticationRoutingModule
  ],
  declarations: [SignUpComponent, LoginComponent, PickerComponent]
})
export class AuthenticationModule { }
