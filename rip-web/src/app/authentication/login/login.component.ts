import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '@app/auth/models';
import { AuthenticationService } from '@app/auth/services';
import { routerTransition } from '@app/core';


@Component({
  selector: 'rip-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition]

})
export class LoginComponent implements OnInit {

  login: FormGroup;
  user: User;
  validation: object;
  isLoginError: boolean;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate([ '/menu' ]);
    }
    this.isLoginError = false;
    this.validation = {
      required: 'Required'
    };
    this.createForm();
  }

  createForm() {
    this.login = this.fb.group({
      username: [ '', Validators.required ],
      password: [ '', Validators.required ],
    });
  }

  onSubmit() {
    if (this.login.invalid) {
      for (let controlName in this.login.controls) {
        this.login.get(controlName)
          .markAsTouched();
      }

      return;
    }

    const preparedPrincipal = this.authService.preparePrincipalData(this.login.value);
    this.authService.loginUser(preparedPrincipal.username, preparedPrincipal.password)
      .subscribe(
        (response) => {
          this.isLoginError = false;
          this.user = response;
          this.authService.fetchUserDetails(preparedPrincipal.username).subscribe(
            (response) => {
              localStorage.setItem('uuid', response.content.uuid);
              window.location.reload(true);
            },
            (error) => {
              console.log(error);
            });
        },
        (error) => {
          this.isLoginError = true;
        });


  }

  get f() {
    return this.login.controls;
  };

}
