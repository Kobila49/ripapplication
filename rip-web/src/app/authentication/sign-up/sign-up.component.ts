import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '@app/auth/models';
import { AuthenticationService } from '@app/auth/services';
import { AppConstants } from '@app/common/app.constants';
import { routerTransition } from '@app/core';


@Component({
  selector: 'rip-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: [ './sign-up.component.scss' ],
  animations: [ routerTransition ]

})
export class SignUpComponent implements OnInit {

  signup: FormGroup;
  user: User;
  validation: object;
  errorMessages: string[];

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.errorMessages = [];
    this.validation = {
      usernamePatternMessage: AppConstants.validationErrorMessages.usernamePattern,
      usernameMinLength: AppConstants.validation.username.minLength,
      namePatternMessage: AppConstants.validationErrorMessages.namePattern,
      nameMinLength: AppConstants.validation.name.minLength,
      surnamePatternMessage: AppConstants.validationErrorMessages.surnamePattern,
      surnameMinLength: AppConstants.validation.surname.minLength,
      passwordMinLength: AppConstants.validation.password.minLength,
      passwordPattern: AppConstants.validationErrorMessages.passwordPattern,
      emailPattern: AppConstants.validationErrorMessages.emailPattern,
      phoneNumberMinLength: AppConstants.validation.phoneNumber.minLength,
      phoneNumberPatternMessage: AppConstants.validationErrorMessages.numberPattern,
      minLengthMessage: AppConstants.validationErrorMessages.minLength,
      required: AppConstants.validationErrorMessages.required
    };
    this.createForm();
  }

  createForm() {
    this.signup = this.fb.group({
      username: [ '',
        [ Validators.required,
          Validators.pattern(AppConstants.validation.username.pattern),
          Validators.minLength(AppConstants.validation.username.minLength) ] ],
      firstName: [ '',
        [ Validators.required,
          Validators.pattern(AppConstants.validation.name.pattern),
          Validators.minLength(AppConstants.validation.name.minLength) ] ],
      lastName: [ '',
        [ Validators.required,
          Validators.pattern(AppConstants.validation.surname.pattern),
          Validators.minLength(AppConstants.validation.surname.minLength) ] ],
      address: [ '', Validators.required ],
      password: [ '',
        [ Validators.required,
          Validators.pattern(AppConstants.validation.password.pattern),
          Validators.minLength(AppConstants.validation.password.minLength) ] ],
      email: [ '', [ Validators.required, Validators.pattern(AppConstants.validation.email.pattern) ] ],
      phoneNumber: [ '',
        [ Validators.required,
          Validators.minLength(AppConstants.validation.phoneNumber.minLength),
          Validators.pattern(AppConstants.validation.phoneNumber.pattern) ] ]
    });
  }

  onSubmit() {
    if (this.signup.invalid) {
      for (let controlName in this.signup.controls) {
        this.signup.get(controlName)
          .markAsTouched();
      }
      ;
      return;
    }
    this.errorMessages = [];
    const preparedUser = this.authService.prepareRegistrationData(this.signup.value);
    this.authService.registerUser(preparedUser)
      .subscribe(
        (response) => {
          this.user = response;
          this.router.navigate([ '/authentication/login' ]);
        },
        (error) => {
          this.errorMessages.push(error.error.message);
          console.log(this.errorMessages);
        });
  }

  get f() {
    return this.signup.controls;
  };
}
