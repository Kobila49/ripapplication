import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private baseUrl: string = `${environment.ripApiUrl}/web`;

  constructor(private httpClient: HttpClient) { }

  fetchUserOrders(): Observable<any> {
    const url = this.baseUrl +"/users/" +localStorage.getItem('uuid')+ "/orders";
    return this.httpClient.get<any>(url);
  }

}
