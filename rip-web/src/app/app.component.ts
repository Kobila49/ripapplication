import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivationEnd, NavigationEnd, Router } from '@angular/router';
import { AuthenticationService } from '@app/auth/services';

import { routerTransition } from '@app/core';
import { environment as env } from '@env/environment';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'rip-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  animations: [ routerTransition ]
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  private broadSub;
  @HostBinding('class') componentCssClass;

  year = new Date().getFullYear();
  logo = require('../assets/images/logo.png');
  navigation = [
    { link: 'home', label: 'O nama' },
    { link: 'working-hours', label: 'Radno vrijeme' },
    { link: 'menu', label: 'Pogledaj MENI & Naruči!' }
  ];
  navigationSideMenu = [
    ...this.navigation
  ];
  isAuthenticated;

  constructor(
    private router: Router,
    private titleService: Title,
    private authService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.broadSub = this.authService.currentAuth.subscribe(_isAuthenticated => {
      this.isAuthenticated = _isAuthenticated;
      this.subscribeToRouterEvents();
    });

  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onLoginClick() {
    this.router.navigate([ 'authentication' ]);
  }

  onLogoutClick() {
    this.authService.logout()
      .subscribe(
        (res) => {
          window.location.reload(true);
          this.router.navigate([ '/home' ]);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  onDetailsClick() {
    this.router.navigate([ '/details/' + localStorage.getItem('uuid') ]);
  }

  onMyPreviousOrdersClick() {
    this.router.navigate([ '/orders/' + localStorage.getItem('uuid') ]);
  }

  private subscribeToRouterEvents() {
    this.router.events
      .pipe(
        takeUntil(this.unsubscribe$),
        filter(
          event =>
            event instanceof ActivationEnd || event instanceof NavigationEnd
        )
      )
      .subscribe(event => {
        if (event instanceof ActivationEnd) {
          this.setPageTitle(event);
        }

        if (event instanceof NavigationEnd) {
          this.trackPageView(event);
        }
      });
  }

  private setPageTitle(event: ActivationEnd) {
    let lastChild = event.snapshot;
    while (lastChild.children.length) {
      lastChild = lastChild.children[ 0 ];
    }
    const { title } = lastChild.data;
    this.titleService.setTitle(
      title ? `${title} - ${env.appName}` : env.appName
    );
  }

  private trackPageView(event: NavigationEnd) {
    (<any>window).ga('set', 'page', event.urlAfterRedirects);
    (<any>window).ga('send', 'pageview');
  }
}

