import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/auth/guards';
import { AccountDetailsComponent } from '@app/static/account-details/account-details.component';
import { AccountOrdersComponent } from '@app/static/account-orders/account-orders.component';
import { HomeComponent } from '@app/static/home/home.component';
import { MenuComponent } from '@app/static/menu/menu.component';
import { WorkingHoursComponent } from '@app/static/working-hours/working-hours.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'O nama' }
  },
  {
    path: 'working-hours',
    component: WorkingHoursComponent,
    data: { title: 'Radno vrijeme' }
  },
  {
    path: 'menu',
    component: MenuComponent,
    canActivate: [ AuthGuard ],
    data: { title: 'Meni' }
  },
  {
    path: 'details/:id',
    component: AccountDetailsComponent,
    canActivate: [ AuthGuard ],
    data: { title: 'Moj profil' }
  },
  {
    path: 'orders/:id',
    component: AccountOrdersComponent,
    canActivate: [ AuthGuard ],
    data: { title: 'Moje narudžbe' }
  }

];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class StaticRoutingModule {
}
