import {Component, OnInit} from '@angular/core';
import {routerTransition} from '@app/core';
import {WorkingDay} from '@app/static/working-hours/working-day.model';

@Component({
  selector: 'rip-working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.scss'],
  animations: [routerTransition]
})
export class WorkingHoursComponent implements OnInit {
  isOpen: boolean;
  workingHours: Array<WorkingDay>;
  todayDay: any;

  constructor() {
  }

  ngOnInit() {
    this.isOpen = false;
    const now = new Date();
    this.workingHours = [
      new WorkingDay('Monday', '10-23'),
      new WorkingDay('Tuesday', '10-23'),
      new WorkingDay('Wednesday', '10-23'),
      new WorkingDay('Thursday', '10-23'),
      new WorkingDay('Friday', '10-23'),
      new WorkingDay('Saturday', '10-23'),
      new WorkingDay('Sunday', '10-23'),
    ];
    this.todayDay = this.workingHours[now.getDay() - 1];
    const hour = now.getHours();
    if (hour > 9 && hour < 23) {
      this.isOpen = true;
    }
  }

}
