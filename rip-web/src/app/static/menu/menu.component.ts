import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {routerTransition} from '@app/core';
import {AppConstants} from '@app/common/app.constants';
import {OrderItem} from '@app/static/account-orders/model/orderItem.model';
import {OrderResource} from '@app/static/menu/model/order-resource';
import {Pizza} from '@app/static/menu/model/pizza.model';
import {MenuService} from '@app/static/shared/menu-service.service';
import {MatDialog} from '@angular/material';
import {ConfirmDialogComponent, ConfirmDialogModel} from '@app/static/menu/confirm-dialog/confirm-dialog.component';
import {AuthenticationService} from '@app/auth/services';
import {User} from '@app/auth/models';


@Component({
  selector: 'rip-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [routerTransition]
})
export class MenuComponent implements OnInit {
  rows: Array<Pizza>;
  messages: object;
  order: Array<Pizza>;
  itemList: OrderResource;
  user: User;
  totalAmount: number;
  IsChecked: boolean;
  IsIndeterminate: boolean;
  LabelAlign: string;
  IsDisabled: boolean;


  constructor(private menuService: MenuService,
              private router: Router,
              public dialog: MatDialog,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.IsChecked = false;
    this.order = [];
    this.rows = [];
    this.itemList = new OrderResource();
    this.itemList.itemList = [];
    this.messages = {
      emptyMessage: AppConstants.tableMessages.emptyMessage,
      totalMessage: AppConstants.tableMessages.totalMessage
    };
    this.getPizzaList();
    this.authService.fetchUserDetails(localStorage.getItem('uuid'))
      .subscribe(
        (response) => {
          this.user = response.content;
        }
      );
  }

  OnChange($event) {
    this.IsChecked = !this.IsChecked;
    if (this.IsChecked) {
      this.totalAmount *= 0.8;
    } else {
      this.totalAmount *= 1.25;
    }
  }

  OnIndeterminateChange($event) {
    console.log($event);
    console.log(this.IsChecked);
  }

  public getPizzaList() {
    this.menuService.fetchPizzaList()
      .subscribe(
        (res) => {
          if (res.content) {
            this.rows = res.content;
            this.rows = [...this.rows];
          }
        },
        (error) => console.log(error)
      );
  }

  addToBucket(row: any) {
    this.order.push(row);
    this.totalAmount = this.calculateTotalAmount();
    this.IsChecked = false;
  }

  clearBucket() {
    this.order = [];
  }

  deleteFromBucket(index: number) {
    this.order.splice(index, 1);
    this.totalAmount = this.calculateTotalAmount();
    this.IsChecked = false;
  }

  makeOrder() {
    for (const o of this.order) {
      const pom = new OrderItem();
      pom.pizzaUuid = o.uuid;
      pom.pizzaCategory = o.pizzaCategory;
      this.itemList.itemList.push(pom);
    }
    this.itemList.discount = this.IsChecked;

    const message = 'Želite li povrditi narudžbu?';

    const dialogData = new ConfirmDialogModel('Potvrdite narudžbu', message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.menuService.makeOrder(this.itemList)
          .subscribe(
            (response) => {
              this.router.navigate(['/orders/' + localStorage.getItem('uuid')]);
            },
            (err) => {
              console.log(err);
            }
          );
      }
    });
  }

  calculateTotalAmount() {
    let pom = 0;
    for (const o of this.order) {
      if (o.pizzaCategory == 'NORMAL') {
        pom += 40;
      } else {
        pom += 70;
      }
    }
    return pom;
  }

}
