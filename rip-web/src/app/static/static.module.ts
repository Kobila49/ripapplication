import {NgModule} from '@angular/core';
import {SharedModule} from '@app/shared';
import {HomeComponent} from '@app/static/home/home.component';
import {StaticRoutingModule} from '@app/static/static-routing.module';
import {WorkingHoursComponent} from './working-hours/working-hours.component';
import {MenuComponent} from './menu/menu.component';
import {AccountDetailsComponent} from './account-details/account-details.component';
import {AccountOrdersComponent} from './account-orders/account-orders.component';
import {ConfirmDialogComponent} from '@app/static/menu/confirm-dialog/confirm-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    StaticRoutingModule
  ],
  declarations: [HomeComponent,
    WorkingHoursComponent,
    MenuComponent,
    AccountDetailsComponent,
    AccountOrdersComponent,
    ConfirmDialogComponent,
  ],
  entryComponents: [ConfirmDialogComponent]
})
export class StaticModule {
}
