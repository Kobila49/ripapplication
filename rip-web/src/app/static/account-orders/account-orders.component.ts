import { Component, OnInit, ViewChild } from '@angular/core';
import { AppConstants } from '@app/common/app.constants';
import { OrdersService } from '@app/shared/services/orders-service.service';
import { Order } from '@app/static/account-orders/model/order.model';

@Component({
  selector: 'rip-account-orders',
  templateUrl: './account-orders.component.html',
  styleUrls: ['./account-orders.component.scss']
})
export class AccountOrdersComponent implements OnInit {
  @ViewChild('ordersTable', {static: false}) table: any;
  rows: Array<Order> ;
  messages: object;
  dateTimeFormat: string;

  constructor(private ordersService: OrdersService) { }

  ngOnInit() {
    this.messages = {
      emptyMessage: AppConstants.tableMessages.emptyMessage,
      totalMessage: AppConstants.tableMessages.totalMessage
    };
    this.dateTimeFormat = AppConstants.clientDateTimeFormat.pipe.dateTime;
    this.getOrders();
  }

  getOrders(){
    this.ordersService.fetchUserOrders().subscribe(
      (response) => {
        this.rows = response.content;
      },
      (error) => {
        console.log(error);
    }
    )
  }

  toggleExpandRow(row): boolean {
    this.table.rowDetail.toggleExpandRow(row);
    return false;
  }
}
