import { User } from '@app/auth/models';
import { OrderItem } from '@app/static/account-orders/model/orderItem.model';

export class Order{
  creationTime: Date;
  orderItems: Array<OrderItem>;
  price: number;
  user: User;
  uuid: string;
}
