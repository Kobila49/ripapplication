import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '@app/static/account-orders/model/order.model';
import { OrderResource } from '@app/static/menu/model/order-resource';
import { PizzaResources } from '@app/static/menu/model/pizza-resources';
import { environment } from '@env/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({ providedIn: 'root' })
export class MenuService {
  private pizzaUrl = '/pizza';
  private baseUrl = `${environment.ripApiUrl}` + '/web';

  constructor(private httpClient: HttpClient) {
  }

  fetchPizzaList(): Observable<PizzaResources> {
    const url = this.baseUrl + this.pizzaUrl;
    return this.httpClient.get<PizzaResources>(url);
  }

  makeOrder(order: OrderResource): Observable<Order> {
    const url = this.baseUrl + '/users/' + localStorage.getItem('uuid') + '/orders';
    return this.httpClient.post<Order>(url, order);
  }
}
