insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-0afb-4a21-97b9-0c539f7c7c05', 'Margharita', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-1afb-4a21-97b9-0c539f7c7c05', 'Margharita', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-2afb-4a21-97b9-0c539f7c7c05', 'Vesuvio', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-3afb-4a21-97b9-0c539f7c7c05', 'Vesuvio', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-4afb-4a21-97b9-0c539f7c7c05', 'Funghi', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-5afb-4a21-97b9-0c539f7c7c05', 'Funghi', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-6afb-4a21-97b9-0c539f7c7c05', 'Capricciosa', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-7afb-4a21-97b9-0c539f7c7c05', 'Capricciosa', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-8afb-4a21-97b9-0c539f7c7c05', 'Slavonska', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-9afb-4a21-97b9-0c539f7c7c05', 'Slavonska', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-0bfb-4a21-97b9-0c539f7c7c05', 'Pikante', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-1bfb-4a21-97b9-0c539f7c7c05', 'Pikante', 'EXCLUSIVE');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-2bfb-4a21-97b9-0c539f7c7c05', 'Quatrro Formaggie', 'NORMAL');
insert into pizzas (uuid, name, pizza_category) values ('2e1d6972-3bfb-4a21-97b9-0c539f7c7c05', 'Quatrro Formaggie', 'EXCLUSIVE');


INSERT INTO role (authority) VALUES ('ROLE_ADMIN');
INSERT INTO role (authority) VALUES ('ROLE_USER');