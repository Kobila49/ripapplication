package hr.tvz.kos.controller.account;


import hr.tvz.kos.dto.request.UserAccountRequestDTO;
import hr.tvz.kos.dto.response.BaseResponse;
import hr.tvz.kos.dto.response.RegisterDTO;
import hr.tvz.kos.dto.response.ResponseCode;
import hr.tvz.kos.dto.response.UserAccountDTO;
import hr.tvz.kos.service.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(path = "/users")
public class AccountController {
	@Autowired
	private UserDetailsServiceImpl userDetailsService;


	@GetMapping("/{username}")
	public ResponseEntity<BaseResponse<UserAccountDTO>> read(@PathVariable String username) {
		UserAccountDTO userAccountDTO = userDetailsService.read(username);

		if (userAccountDTO == null) {
			return ResponseEntity.notFound().build();
		}
		else {
			BaseResponse<UserAccountDTO> baseResponseDTO = new BaseResponse<>();
			baseResponseDTO.setContent(userAccountDTO);
			baseResponseDTO.setResponseCode(ResponseCode.OK);

			return ResponseEntity.status(HttpStatus.OK).body(baseResponseDTO);
		}

	}

	@PostMapping("/register")
	public ResponseEntity<BaseResponse<RegisterDTO>> registerUser(@RequestBody @Valid UserAccountRequestDTO userAccountRequestDTO) {

		RegisterDTO registerDTO = userDetailsService.register(userAccountRequestDTO);

		BaseResponse<RegisterDTO> baseResponseDTO = new BaseResponse<>();
		baseResponseDTO.setContent(registerDTO);
		baseResponseDTO.setResponseCode(ResponseCode.OK);

		return ResponseEntity.status(HttpStatus.OK).body(baseResponseDTO);

	}

	@PutMapping("/modify/{userUuid}")
	public ResponseEntity<BaseResponse<RegisterDTO>> modifyUser(@PathVariable(value = "userUuid") String userUuid, @RequestBody @Valid UserAccountRequestDTO userAccountRequestDTO) {
		RegisterDTO registerDTO = userDetailsService.modify(userUuid,userAccountRequestDTO);

		BaseResponse<RegisterDTO> baseResponseDTO = new BaseResponse<>();
		baseResponseDTO.setContent(registerDTO);
		baseResponseDTO.setResponseCode(ResponseCode.OK);

		return ResponseEntity.status(HttpStatus.OK).body(baseResponseDTO);
	}


}
