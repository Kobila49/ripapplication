package hr.tvz.kos.controller.order;

import hr.tvz.kos.dto.request.OrderRequestDTO;
import hr.tvz.kos.dto.response.BaseResponse;
import hr.tvz.kos.dto.response.OrderDTO;
import hr.tvz.kos.dto.response.PizzaDTO;
import hr.tvz.kos.dto.response.ResponseCode;
import hr.tvz.kos.service.OrderService;
import hr.tvz.kos.service.PizzaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/web")
public class OrderController {
	@Autowired
	OrderService orderService;

	@Autowired
	PizzaService pizzaService;


	@RequestMapping(value = "/users/{userUuid}/orders", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse<List<OrderDTO>>> getOrders(@PathVariable(value = "userUuid") String userUuid) {
		List<OrderDTO> orders = orderService.getUserOrders(userUuid);
		Boolean pom = orders == null;

		if (orders.size() == 0) {
			return ResponseEntity.noContent().build();
		}
		else {
			BaseResponse<List<OrderDTO>> getOrdersResponseDTO = new BaseResponse<>();
			getOrdersResponseDTO.setContent(orders);
			getOrdersResponseDTO.setResponseCode(ResponseCode.OK);

			return ResponseEntity.status(HttpStatus.OK).body(getOrdersResponseDTO);
		}
	}


	@RequestMapping(value = "/users/{userUuid}/orders", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse<OrderDTO>> createOrder(@PathVariable(value = "userUuid") String userUuid,
			@Valid @RequestBody OrderRequestDTO orderRequestDTO) {

		OrderDTO orderDTO = orderService.createOrder(userUuid, orderRequestDTO);

		BaseResponse<OrderDTO> baseResponseDTO = new BaseResponse<>();
		baseResponseDTO.setContent(orderDTO);
		baseResponseDTO.setResponseCode(ResponseCode.OK);

		return ResponseEntity.status(HttpStatus.OK).body(baseResponseDTO);
	}

	@RequestMapping(value = "/orders/{orderUuid}", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse<OrderDTO>> getOrder(@PathVariable(value = "orderUuid") String orderUuid) {

		OrderDTO order = orderService.getOrder(orderUuid);

		BaseResponse<OrderDTO> getOrderResponseDTO = new BaseResponse<>();
		getOrderResponseDTO.setContent(order);
		getOrderResponseDTO.setResponseCode(ResponseCode.OK);

		return ResponseEntity.status(HttpStatus.OK).body(getOrderResponseDTO);
	}


	@RequestMapping(path = "/pizza", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse<List<PizzaDTO>>> findAll() {
		List<PizzaDTO> pizzaDTOS = pizzaService.findAll();

		BaseResponse<List<PizzaDTO>> pizzaResponseDTO = new BaseResponse<>();
		pizzaResponseDTO.setContent(pizzaDTOS);
		pizzaResponseDTO.setResponseCode(ResponseCode.OK);


		return ResponseEntity.status(HttpStatus.OK).body(pizzaResponseDTO);
	}


}
