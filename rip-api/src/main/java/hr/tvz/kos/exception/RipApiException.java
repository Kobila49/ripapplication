package hr.tvz.kos.exception;

public class RipApiException extends RuntimeException {
    private static final long serialVersionUID = 3789252423033362059L;

    public RipApiException() {}

    public RipApiException(String message) {
        super(message);
    }
}
