package hr.tvz.kos.mapper;


import hr.tvz.kos.dto.request.UserAccountRequestDTO;
import hr.tvz.kos.dto.response.UserAccountDTO;
import hr.tvz.kos.entity.UserAccount;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserAccountMapper {

	UserAccount userAccountDtoToUserAccount(UserAccountRequestDTO userAccountRequestDTO);

	UserAccountDTO userAccountToUserAccountDto(UserAccount userAccount);
}
