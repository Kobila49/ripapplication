package hr.tvz.kos.mapper;

import hr.tvz.kos.dto.response.OrderItemDTO;
import hr.tvz.kos.entity.OrderItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderItemMapper {

	@Mappings({
			@Mapping(source = "pizza.uuid", target = "pizzaUuid"),
			@Mapping(source = "pizza.name", target = "pizzaName")
	})
	OrderItemDTO orderItemToOrderItemDto(OrderItem orderItem);
}
