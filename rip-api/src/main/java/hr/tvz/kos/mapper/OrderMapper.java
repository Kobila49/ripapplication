package hr.tvz.kos.mapper;

import hr.tvz.kos.dto.response.OrderDTO;
import hr.tvz.kos.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel="spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderMapper {

	Order orderDtoToOrder(OrderDTO orderDTO);


	OrderDTO orderToOrderDto(Order order);
}
