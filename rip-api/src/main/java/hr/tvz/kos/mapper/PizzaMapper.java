package hr.tvz.kos.mapper;

import hr.tvz.kos.dto.response.PizzaDTO;
import hr.tvz.kos.entity.Pizza;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PizzaMapper {

	@Mappings({
			@Mapping(source = "uuid", target = "uuid"),
			@Mapping(source = "name", target = "name"),
			@Mapping(source = "pizzaCategory", target = "pizzaCategory")
	})
	PizzaDTO pizzaToPizzaDTO(Pizza pizza);
}