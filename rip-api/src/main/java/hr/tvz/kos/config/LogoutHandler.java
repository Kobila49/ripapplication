package hr.tvz.kos.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class LogoutHandler implements LogoutSuccessHandler {

	@Override
	public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
		try {
			SecurityContextHolder.getContext().setAuthentication(null);
			SecurityContextHolder.clearContext();
			String responseValue = new ObjectMapper().writeValueAsString("success");
			httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
			httpServletResponse.addHeader("Content-Type", "application/json");
			httpServletResponse.getWriter().print(responseValue);
		}
		catch (Exception e) {
			log.error("Error", e);
			String responseValue;
			try {
				responseValue = new ObjectMapper().writeValueAsString("failed");
				httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				httpServletResponse.addHeader("Content-Type", "application/json");
				httpServletResponse.getWriter().print(responseValue);
			}
			catch (IOException e1) {
				log.error("Error", e1);
			}
		}
	}
}
