package hr.tvz.kos.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class AppProperties {

    @Value("${ripapi.price.exclusive}")
    private Integer exclusivePrice;
    @Value("${ripapi.price.normal}")
    private Integer normalPrice;


    @Value("${ripapi.points.exclusive}")
    private Integer pointsExclusive;
    @Value("${ripapi.points.normal}")
    private Integer pointsNormal;


}
