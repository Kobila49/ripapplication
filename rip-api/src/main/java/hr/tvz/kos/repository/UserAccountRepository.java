package hr.tvz.kos.repository;

import hr.tvz.kos.entity.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

     UserAccount findByUsername(String username);

     UserAccount findByEmail(String email);

     UserAccount findByUuid(String uuid);

}