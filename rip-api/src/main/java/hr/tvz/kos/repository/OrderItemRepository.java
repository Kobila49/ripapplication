package hr.tvz.kos.repository;

import hr.tvz.kos.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
	List<OrderItem> findAllByOrder_Id(int order_id);
}
