package hr.tvz.kos.repository;

import hr.tvz.kos.entity.Order;
import hr.tvz.kos.entity.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order findByUuid(String uuid);

    List<Order> findByUserAccountOrderByCreateTimeDesc(UserAccount userAccount);

}
