package hr.tvz.kos.repository;

import hr.tvz.kos.entity.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PizzaRepository extends JpaRepository<Pizza, Long> {

	List<Pizza> findAll();

	Pizza findByUuid(String uuid);
}
