package hr.tvz.kos.service;

import hr.tvz.kos.dto.request.UserAccountRequestDTO;
import hr.tvz.kos.dto.response.RegisterDTO;
import hr.tvz.kos.dto.response.UserAccountDTO;
import hr.tvz.kos.entity.Role;
import hr.tvz.kos.entity.UserAccount;
import hr.tvz.kos.exception.RipApiException;
import hr.tvz.kos.mapper.UserAccountMapper;
import hr.tvz.kos.repository.RoleRepository;
import hr.tvz.kos.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserAccountMapper userAccountMapper;

	@Override
	public UserDetails loadUserByUsername(String username) {
		UserAccount applicationUser = userAccountRepository.findByUsername(username);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(username);
		}

		return new User(applicationUser.getUsername(), applicationUser.getPassword(), applicationUser.getRoles());
	}

	public UserAccountDTO read(String username) {
		UserAccount userAccountByUsername = userAccountRepository.findByUsername(username);
		UserAccount userAccountByUuid = userAccountRepository.findByUuid(username);

		if (userAccountByUsername == null && userAccountByUuid == null) {
			return null;
		}
		else if (userAccountByUsername != null) {
			UserAccountDTO userDTO = userAccountMapper.userAccountToUserAccountDto(userAccountByUsername);
			return userDTO;
		} else {
			UserAccountDTO userDTO = userAccountMapper.userAccountToUserAccountDto(userAccountByUuid);
			return userDTO;
		}
	}

	@Transactional
	public RegisterDTO register(UserAccountRequestDTO userAccountRequestDTO) {

		RegisterDTO registerDTO = new RegisterDTO();
		if (userAccountRepository.findByUsername(userAccountRequestDTO.getUsername()) != null) {
			throw new RipApiException("Username is taken");
		}

		if (userAccountRepository.findByEmail(userAccountRequestDTO.getEmail()) != null) {
			throw new RipApiException("Email is already used");
		}

		UserAccount userAccount = userAccountMapper.userAccountDtoToUserAccount(userAccountRequestDTO);
		userAccount.setPassword(bCryptPasswordEncoder.encode(userAccountRequestDTO.getPassword()));
		userAccount.setUuid(UUID.randomUUID().toString());
		userAccount.setPoints(0);

		Role role = roleRepository.findByAuthority("ROLE_USER");
		Set<Role> roles = new HashSet<>();
		roles.add(role);


		userAccount.setRoles(roles);
		userAccount = userAccountRepository.saveAndFlush(userAccount);
		registerDTO.setUserUuid(userAccount.getUuid());

		return registerDTO;

	}

	@Transactional
	public RegisterDTO modify(String userUuid,UserAccountRequestDTO userAccountRequestDTO) {
		RegisterDTO registerDTO = new RegisterDTO();

		UserAccount oldUser = userAccountRepository.findByUuid(userUuid);
		oldUser.setAddress(userAccountRequestDTO.getAddress());
		oldUser.setPassword(bCryptPasswordEncoder.encode(userAccountRequestDTO.getPassword()));
		oldUser.setPhoneNumber(userAccountRequestDTO.getPhoneNumber());
		oldUser = userAccountRepository.save(oldUser);

		registerDTO.setUserUuid(oldUser.getUuid());
		return registerDTO;
	}
}
