package hr.tvz.kos.service;


import hr.tvz.kos.config.AppProperties;
import hr.tvz.kos.dto.request.OrderItemRequestDTO;
import hr.tvz.kos.dto.request.OrderRequestDTO;
import hr.tvz.kos.dto.response.OrderDTO;
import hr.tvz.kos.dto.response.OrderItemDTO;
import hr.tvz.kos.dto.response.UserAccountDTO;
import hr.tvz.kos.entity.Order;
import hr.tvz.kos.entity.OrderItem;
import hr.tvz.kos.entity.Pizza;
import hr.tvz.kos.entity.UserAccount;
import hr.tvz.kos.exception.RipApiException;
import hr.tvz.kos.mapper.OrderItemMapper;
import hr.tvz.kos.mapper.OrderMapper;
import hr.tvz.kos.mapper.PizzaMapper;
import hr.tvz.kos.mapper.UserAccountMapper;
import hr.tvz.kos.repository.OrderItemRepository;
import hr.tvz.kos.repository.OrderRepository;
import hr.tvz.kos.repository.PizzaRepository;
import hr.tvz.kos.repository.UserAccountRepository;
import hr.tvz.kos.type.PizzaCategory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private PizzaRepository pizzaRepository;

	@Autowired
	private UserAccountRepository userAccountRepository;

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private UserAccountMapper userAccountMapper;

	@Autowired
	private PizzaMapper pizzaMapper;

	@Autowired
	private OrderItemMapper orderItemMapper;

	@Autowired
	private AppProperties config;

	@PreAuthorize("hasRole('ROLE_USER')")
	public List<OrderDTO> getUserOrders(String userUuid) {

		UserAccount userAccount = userAccountRepository.findByUuid(userUuid);
		if(userAccount == null)
			throw new RipApiException("UserAccount does not exist for provided userAccount uuid");

		List<Order> orders = orderRepository.findByUserAccountOrderByCreateTimeDesc(userAccount);

//		if(orders.isEmpty())
//			throw new RipApiException("No orders exist for provided userAccount");

		List<OrderDTO> ordersDto = new ArrayList<>();
		for(Order order: orders) {
			OrderDTO orderDto = orderMapper.orderToOrderDto(order);
			List<OrderItem> orderItems = orderItemRepository.findAllByOrder_Id(order.getId());
			List<OrderItemDTO> orderItemDTOS = new ArrayList<>();
			for(OrderItem orderItem: orderItems) {
				OrderItemDTO orderItemDTO = orderItemMapper.orderItemToOrderItemDto(orderItem);
				orderItemDTOS.add(orderItemDTO);
			}
			orderDto.setOrderItems(orderItemDTOS);
			UserAccountDTO userDTO = userAccountMapper.userAccountToUserAccountDto(userAccount);
			orderDto.setUser(userDTO);
			ordersDto.add(orderDto);
			orderDto.setPrice(order.getPrice());
			orderDto.setCreationTime(order.getCreateTime());
		}

		return ordersDto;
	}

	@PreAuthorize("hasRole('ROLE_USER')")
	public OrderDTO getOrder(String orderUuid) {

		Order order = orderRepository.findByUuid(orderUuid);
		if(order == null)
			throw new RipApiException("Order does not exist for provided order uuid");

		OrderDTO orderDto = orderMapper.orderToOrderDto(order);

		List<OrderItemDTO> orderItemDTOS = new ArrayList<>();
		for(OrderItem orderItem : order.getOrderItems()) {
			OrderItemDTO orderItemDTO = orderItemMapper.orderItemToOrderItemDto(orderItem);
			orderItemDTOS.add(orderItemDTO);
		}

		if(!orderItemDTOS.isEmpty())
			orderDto.setOrderItems(orderItemDTOS);

		orderDto.setPrice(order.getPrice());
		orderDto.setCreationTime(order.getCreateTime());

		return orderDto;

	}


	@PreAuthorize("hasRole('ROLE_USER')")
	public OrderDTO createOrder(String userUuid, OrderRequestDTO orderRequestDTO) {

		UserAccount userAccount = userAccountRepository.findByUuid(userUuid);
		if(userAccount == null)
			throw new RipApiException("UserAccount does not exist for provided userAccount uuid");


		Order order = new Order();
		order.setCreateTime(ZonedDateTime.now());


		Double totalPrice = 0.0d;
		Integer userPoints = userAccount.getPoints();

		List<OrderItem> orderItems = new ArrayList<>();

		for (OrderItemRequestDTO orderItemRequestDTO : orderRequestDTO.getItemList()) {

			OrderItem orderItem = new OrderItem();

			Pizza pizza = pizzaRepository.findByUuid(orderItemRequestDTO.getPizzaUuid());
			if(pizza == null)
				throw new RipApiException("Item does not exist for provided item uuid");

			orderItem.setPizza(pizza);
			orderItem.setPizzaCategory(orderItemRequestDTO.getPizzaCategory());
			orderItem.setUuid(UUID.randomUUID().toString());


			PizzaCategory category = orderItem.getPizzaCategory();
			if(category == PizzaCategory.EXCLUSIVE) {
				totalPrice += config.getExclusivePrice();
				userPoints += config.getPointsExclusive();
			}
			else {
				totalPrice += config.getNormalPrice();
				userPoints += config.getPointsNormal();
			}

			orderItems.add(orderItem);
		}

		userAccount.setPoints(userPoints);
		userAccount = userAccountRepository.save(userAccount);

		order.setOrderItems(orderItems);
		order.setPrice(new BigDecimal(totalPrice));
		order.setUuid(UUID.randomUUID().toString());
		order.setUserAccount(userAccount);
		order = orderRepository.saveAndFlush(order);

		for(OrderItem i : orderItems) {
			i.setOrder(order);
		}
		orderItemRepository.saveAll(orderItems);

		UserAccountDTO userDTO = userAccountMapper.userAccountToUserAccountDto(userAccount);

		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setUuid(order.getUuid());
		orderDTO.setUser(userDTO);
		List<OrderItemDTO> orderItemDTOS = new ArrayList<>();
		for (OrderItem orderItem : orderItems
		) {
			orderItemDTOS.add(orderItemMapper.orderItemToOrderItemDto(orderItem));
		}
		orderDTO.setOrderItems(orderItemDTOS);
		orderDTO.setPrice(order.getPrice());
		orderDTO.setCreationTime(order.getCreateTime());

		return orderDTO;

	}
}
