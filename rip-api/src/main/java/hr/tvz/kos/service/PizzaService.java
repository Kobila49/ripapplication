package hr.tvz.kos.service;

import hr.tvz.kos.dto.response.PizzaDTO;
import hr.tvz.kos.entity.Pizza;
import hr.tvz.kos.mapper.PizzaMapper;
import hr.tvz.kos.repository.PizzaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PizzaService {

    @Autowired
    PizzaRepository pizzaRepository;

    @Autowired
    PizzaMapper pizzaMapper;

    public List<PizzaDTO> findAll() {
        List<Pizza> pom = pizzaRepository.findAll();
        List<PizzaDTO> pomDTO = new ArrayList<>();

        for(Pizza p: pom) {
            pomDTO.add(pizzaMapper.pizzaToPizzaDTO(p));
        }

        return pomDTO;
    }
}
