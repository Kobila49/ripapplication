package hr.tvz.kos.type;

/**
 * Enum defines pizza categories
 * 
 * @author Igor Kos
 */

public enum PizzaCategory {
	
	NORMAL,
	EXCLUSIVE;
	
}
