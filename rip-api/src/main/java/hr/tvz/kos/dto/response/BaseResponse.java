package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class BaseResponse<T> {

	private ResponseCode responseCode;

	private String responseMessage;

	private String errorMessage;

	private T content;

}
