package hr.tvz.kos.dto.response;

public enum ResponseCode {

	OK, INFO, ERROR
}
