package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDTO {

	private String uuid;

	private ZonedDateTime creationTime;

	private UserAccountDTO user;

	private List<OrderItemDTO> orderItems;

	private BigDecimal price;
}
