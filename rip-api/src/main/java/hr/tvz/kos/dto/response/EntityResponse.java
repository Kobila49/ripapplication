package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EntityResponse extends BaseResponse {

	private List<FieldMessage> responseFieldMessages;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((responseFieldMessages == null) ? 0 : responseFieldMessages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityResponse other = (EntityResponse) obj;
		if (responseFieldMessages == null) {
			if (other.responseFieldMessages != null)
				return false;
		} else if (!responseFieldMessages.equals(other.responseFieldMessages))
			return false;
		return true;
	}
}
