package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Comparator;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldMessage implements Comparable<Object> {

	private String field;

	private String message;

	@Override
	public int compareTo(Object anotherFieldMessage) {

		return Comparator.comparing(FieldMessage::getField)
				.thenComparing(FieldMessage::getMessage)
				.compare(this, (FieldMessage)anotherFieldMessage);
	}
}
