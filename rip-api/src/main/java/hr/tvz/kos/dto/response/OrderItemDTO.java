package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import hr.tvz.kos.type.PizzaCategory;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderItemDTO {

	private PizzaCategory pizzaCategory;

	private String pizzaName;

	private String pizzaUuid;
}
