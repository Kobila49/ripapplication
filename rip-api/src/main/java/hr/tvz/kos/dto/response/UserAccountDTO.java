package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountDTO {

	private String uuid;

	private String username;

	private String firstName;

	private String lastName;

	private String address;

	private String email;

	private String phoneNumber;
}
