package hr.tvz.kos.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountRequestDTO {

	@NotNull(message = "Username is mandatory")
	private String username;

	@NotNull(message = "First name is mandatory")
	private String firstName;

	@NotNull(message = "Last name is mandatory")
	private String lastName;

	@NotNull(message="Address is mandatory")
	private String address;

	@NotNull(message = "Password is mandatory")
	@Size(min = 6, message = "Password must have at least 6 chars")
	private String password;

	@NotNull(message = "Email is mandatory")
	@Email(message = "Email is not in good format")
	private String email;

	@NotNull(message="Phone number is mandatory")
	@Size(min=8, message="Phone number must have at least 8 chars")
	private String phoneNumber;
}
