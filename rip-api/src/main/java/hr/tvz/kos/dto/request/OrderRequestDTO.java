package hr.tvz.kos.dto.request;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OrderRequestDTO {

	@Valid
	@NotNull(message="Item list should contain at least one item")
	private List<OrderItemRequestDTO> itemList;

}
