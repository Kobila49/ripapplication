package hr.tvz.kos.dto.request;

import hr.tvz.kos.type.PizzaCategory;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OrderItemRequestDTO {

	@NotNull(message="PizzaCategory is mandatory")
	private PizzaCategory pizzaCategory;

	@NotNull(message="PizzaUuid uuid is mandatory")
	private String pizzaUuid;

}