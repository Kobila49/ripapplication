package hr.tvz.kos.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PizzaDTO {

	private String uuid;

	private String name;

	private String pizzaCategory;
}
