package hr.tvz.kos.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Table(name = "orders")
public class Order extends BaseEntity {

	@Column(nullable = false, unique = true)
	private String uuid;

	@Column(nullable = false, name="create_time")
	private ZonedDateTime createTime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_account_id", referencedColumnName = "id", nullable = false)
	@JsonBackReference(value = "order_user_reference")
	private UserAccount userAccount;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	@JsonManagedReference(value = "item_order_reference")
	private List<OrderItem> orderItems;

	@Column(name = "price", nullable = false)
	private BigDecimal price;

}
