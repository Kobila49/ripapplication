package hr.tvz.kos.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role extends BaseEntity implements GrantedAuthority {

	@Getter
	@Setter
	@Column(nullable = false, unique = true)
	private String authority;
}
