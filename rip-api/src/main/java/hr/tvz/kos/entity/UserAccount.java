package hr.tvz.kos.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter
@Setter
@Table(name = "user_account")
public class UserAccount extends BaseEntity {

	@Column(nullable = false, unique = true)
	private String uuid;

	@Column(unique = true)
	private String username;

	@Column(nullable = false, name = "first_name")
	private String firstName;

	@Column(nullable = false, name = "last_name")
	private String lastName;

	@Column(nullable = false)
	private String address;

	@Column(nullable = false, name = "password")
	private String password;

	@Column(nullable = false, name = "email", unique = true)
	private String email;

	@Column(nullable = false, name = "phone_number")
	private String phoneNumber;

	@Column(nullable = false)
	private Integer points;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "userAccount")
	@JsonManagedReference(value = "order_user_reference")
	private Set<Order> orders;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role",
			joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<Role> roles;


}