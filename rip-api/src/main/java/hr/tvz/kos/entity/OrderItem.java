package hr.tvz.kos.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hr.tvz.kos.type.PizzaCategory;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Table(name = "order_item")
public class OrderItem extends BaseEntity {

	@Column(nullable = false, unique = true)
	private String uuid;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, name = "category")
	private PizzaCategory pizzaCategory;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", referencedColumnName = "id", nullable = false)
	@JsonBackReference(value = "item_order_reference")
	private Order order;

	@OneToOne(fetch = FetchType.LAZY)
	private Pizza pizza;

}
