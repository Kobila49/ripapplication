package hr.tvz.kos.entity;

import hr.tvz.kos.type.PizzaCategory;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Table(name = "pizzas")
public class Pizza extends BaseEntity {

	@Column(nullable = false, unique = true)
	private String uuid;

	@Column(nullable = false)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private PizzaCategory pizzaCategory;
}
