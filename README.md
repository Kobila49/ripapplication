## Rip Online Order application ##
This is pizza restaurant ordering application .

## What is this repository for? ##
In this repository Java backend code is stored with REST API, business logic, entities and tests. Also there is code for front end Angular application.

## How do I get set up? ##

## Backend: ##
### 1. run src/main/resources/sql/orders-DDL.sql to create DB ###
### 2. set 'ddl-auto: create' in src/main/resources/application.yml ###
### 3. in project root directory run: 'mvnw spring-boot:run' ###
### 4. run src/main/resources/sql/orders-DML.sql to fill DB with data ###
### 5. set 'ddl-auto: validate' in src/main/resources/application.yml ###

## Frontend: ##
### 1. Install latest version of nodeJs ( https://nodejs.org/en/download/ ) ###
### 2. Go to location of rip-web and enter the rip-web folder( cd rip-web) ###
### 3. Run command npm install ###
### 4. After it is done run command npm start ###
### 5. Open localhost:4200 in your browser ###


## About application ##
Application simulates pizza ordering through restaurant online web page.

## Possible improvements ##
There is still many services which can be added to make application more smarter such as online payment and other stuff.